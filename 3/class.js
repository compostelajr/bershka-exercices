// CSS colored console only works for browsers, looks weird in node

class myClass {
    constructor(message) {
        console.log('%c constructor start ...', 'color:pink; background-color:midnightblue;')
        this.message = '';
        this.setMessage(message);
        this.getMessage();
        this.writeMessage();
        console.log('%c constructor end', 'color:pink; background-color:midnightblue;')
    }

    setMessage(msg) {
        if (typeof(msg) !== 'string') throw `Input is NOT a string: ${msg} (${typeof(msg)})`;
        else this.message = msg;
    }
    getMessage() {
        return this.message;
    }
    writeMessage() {
        console.log(this.message);
    }

};

let member = new myClass('Initial message!');
console.log('\n')
console.log(member.getMessage());