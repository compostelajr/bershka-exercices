// CSS colored console only works for browsers, looks weird in node

let myModule = (function(msg) {
    console.log('%c self executing...', 'color:pink; background-color:midnightblue;')
    let message = '';
    console.log(`message is: ${(message.length < 0 ? message : 'no message yet')}`);

    
    let methods = {
        setMessage: function setMessage(inputMessage) {
            console.log(`executing setMessage() with input: "${inputMessage}" ...`);
            if (typeof(inputMessage) !== 'string') throw `Input is NOT a string: ${inputMessage} (${typeof(inputMessage)})`;
            message = inputMessage;
        },
        getMessage: function getMessage() {
            console.log('executing getMessage() ...');
            return message;
        },
        writeMessage: function writeMessage() {
            console.log('executing writeMessage() ...');
            console.log(message);
        }
    }
    methods.setMessage(msg);
    methods.getMessage();
    methods.writeMessage();

    return methods;
})("This is my message preset. Call setMessage(msg) to change it!")

console.log('\n');
console.log('%c Now we will call a function of myModule', 'color:pink; background-color:midnightblue;');
myModule.setMessage('This is the new message!');
myModule.writeMessage();