'use strict';

function getHighestValueFromArray(inputArr) {
    return inputArr.reduce((prev, current) => {
        // Throw exception if current item is not a number
        if (typeof(current) !== "number") throw `Item ${current} is NOT a number (${typeof(current)})`;
        // Check for null, if no prev is submited, defaults to 0
        if (prev === null || prev < current) return current;
        else return prev;
    }, null)
};

console.log(getHighestValueFromArray([-4,1,15,9,-8,1,23,18-3,4,13]));