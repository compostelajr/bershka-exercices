# Question 5, the CSS position property.

> Explain the differences on the usage of *relative*, *fixed* and *absolute* as positioning methods of the CSS position property.

---

## Explanation

Regular elements have static class applied by default. This means that the blocks behave normally, following the document flow.

#### position: relative

`position: relative;` means that the item positions itself relative to where it should be if it had position: static. This means that an item with `{position: relative; bottom: 10px;}` will position itself 10px above its normal position. The item will still follow page flow regularly.

#### position: absolute

`position: absolute;` means that it **does not** follow page flow. It allows you to position the item anywhere on the screen, exactly where you tell it to be. The position scope is relative to the closest parent with either relative or absolute position.

#### position: fixed

`position: fixed;` means that the item will be positioned absolutely **relative to the viewport**.